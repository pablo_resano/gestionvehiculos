package clases;

import java.time.LocalDate;
	/**
	 * Clase que actua como un almacen de pedidos
	 * @author DAW
	 *
	 */
public class Vehiculo {
	private Albaran[] albaranes;
	
	/**
	 * constructor de un nuevo Vehiculo 
	 * @param maxAlbaranes indica cual es la cantidad maxima de albaranes para este vehiculo
	 * 
	 */
	
	public Vehiculo(int maxAlbaranes) {
		this.albaranes = new Albaran[maxAlbaranes];
	}
	
	/**
	 * da de alta un nuevo albaran para un vehiculo ya creado
	 * @param codAlbaran que tendra el albaran
	 * @param precio del albaran
	 * @param codLibro que contiene el albaran
	 * La fecha se asigna con LocalDate.now()
	 */
	public  void altaAlbaran(String codAlbaran, double precio, String codVehiculo){
		for (int i = 0; i < albaranes.length; i++) {
			if (albaranes[i] == null) {
				albaranes[i] = new Albaran(codAlbaran);
				albaranes[i].setPrecio(precio);
				albaranes[i].setCodVehiculo(codVehiculo);
				albaranes[i].setFecha(LocalDate.now());
				break;
			}
			
		}
		
	}
	
	/**
	 * busca un determinado albaran dado un codVehiculo
	 * @param codAlbaran que se quiere buscar
	 * @return objeto Albaran
	 */
	public Albaran buscarAlbaran(String codVehiculo) {
		for (int i = 0; i < albaranes.length; i++) {
			if (albaranes[i] != null) {
				if (albaranes[i].getCodAlbaran().equals(codVehiculo)) {
					return albaranes[i];
				}
			}
		}
		return null;
	}
	
	/**
	 * Elimina los albaranes con el codigo introducido
	 * @param codAlbaran que se desea eliminar
	 */
	public void eliminarAlbaran(String codAlbaran) {
		for (int i = 0; i < albaranes.length; i++) {
			if (albaranes[i] != null) {
				if (albaranes[i].getCodAlbaran().equals(codAlbaran)) {
					albaranes[i]=null;
				}
			}
		}
	}
	
	/**
	 * Pinta por consola todos los alabaranes
	 */
	public void listarAlbaranes() {
		for (int i = 0; i < albaranes.length; i++) {
			if (albaranes[i] != null) {
					System.out.println(albaranes[i]);
				
			}
		}
	}
	
	/**
	 * Cambia un vehiculo de un albaran determinado
	 * @param codAlbaran del que se quiere cambiar el vehiculo
	 * @param codVehiculo2 codigo del nuevo vehiculo que reemplazara al anterior
	 */
	public void cambiarAlbaran(String codAlbaran, String codVehiculo2) {
		for (int i = 0; i < albaranes.length; i++) {
			if (albaranes[i] != null) {
				if (albaranes[i].getCodAlbaran().equals(codAlbaran)) {
					albaranes[i].setCodVehiculo(codVehiculo2);
				}
			}
		}
	}
	
	/**
	 * pinta por consola una lista con los albaranes que contengan el vehiculo introducido
	 * @param codAlbaran del que se quiere mostrar la lista de albaranes
	 */
	public void listarAlbaranesPorVehiculo(String codVehiculo) {
		for (int i = 0; i < albaranes.length; i++) {
			if (albaranes[i] != null) {
				if (albaranes[i].getCodVehiculo().equals(codVehiculo)) {
					System.out.println(albaranes[i]);
				}
			}
		}
	}
	
	
}
