package test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.Albaran;
import clases.Vehiculo;

class Tests {

	static Vehiculo vehiculo1;
	private Albaran actual;
	private String esperado;

	@BeforeAll
	static void BeforeAll() {

		vehiculo1 = new Vehiculo(0);
	}

	@BeforeEach

	void setUp() {
		
		vehiculo1 = new Vehiculo(1);
		vehiculo1.altaAlbaran("A1", 4.51, "A");
		actual = vehiculo1.buscarAlbaran("A1");
		esperado = "A1";
	}

	@AfterEach
	void tearDown() {
		
		vehiculo1 = new Vehiculo(0);
	}


	@Test
	public void altaAlbaran() {

		assertNotEquals(esperado, actual.getCodAlbaran());
	}

	@Test
	public void altaAlbaranExistente() {

		assertEquals(esperado, actual.getCodAlbaran());
	}

	@Test
	void testBuscarAlbaran() {

		assertEquals(esperado, actual.getCodAlbaran());
	}

	@Test

	void testAlbaranInexistente() {

		assertNotEquals(esperado, actual.getCodAlbaran());
	}

	@Test
	void testEliminarAlbaran() {

		assertEquals(esperado, actual.getCodAlbaran());
	}

	@Test

	void testAlbaranEliminarInexistente() {

		assertNotEquals(esperado, actual.getCodAlbaran());

	}

	@Test

	void testCambiarAlbaran() {

		assertEquals(esperado, actual.getCodAlbaran());
	}

	@Test

	void testCambiarAlbaraninexistente() {

		assertNotEquals(esperado, actual.getCodAlbaran());
	}

	@Test

	void testComprobarCambiarAlbaran() {

		vehiculo1.cambiarAlbaran("A1", "B");
		esperado = "B";
		assertEquals(esperado, actual.getCodVehiculo());
	}

	@Test

	void testComprobarNoCambiarAlbaran() {

		vehiculo1.cambiarAlbaran("A1", "B");
		esperado = "B";
		assertNotEquals(esperado, actual.getCodVehiculo());
	}

	@Test

	void testListarAlbaranesporVehiculo() {

		esperado = "A";
		assertEquals(esperado, actual.getCodVehiculo());

	}

	@Test

	void testListarAlbaranesporVehiculoInexistente() {

		esperado = "C";
		assertNotEquals(esperado, actual.getCodVehiculo());

	}

}
